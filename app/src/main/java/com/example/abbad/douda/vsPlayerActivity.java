package com.example.abbad.douda;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.DrawableRes;
import androidx.appcompat.app.AppCompatActivity;

public class vsPlayerActivity extends AppCompatActivity {
    int nlive;

    MediaPlayer drop = null;
    MediaPlayer select = null;
    MediaPlayer move = null;
    MediaPlayer str = null;
    MediaPlayer success = null;
    MediaPlayer deny = null;
    Board board = new Board();
    Piece p;
    List<Piece> PieceList = new ArrayList<Piece>();
    Player player1 = new Player(0);
    Player player2 = new Player(1);
@DrawableRes int restId;
    int moveFrom = -1;
    ImageView pastCounter;
    int ptCounter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vs_player);
        final SharedPreferences prefImg = getSharedPreferences("IMG", Context.MODE_PRIVATE);
        restId=prefImg.getInt("img",R.drawable.blue );

        drop = MediaPlayer.create(this, R.raw.drop);
        select = MediaPlayer.create(this, R.raw.select);
        move = MediaPlayer.create(this, R.raw.mov);
        str = MediaPlayer.create(this, R.raw.str);
        deny = MediaPlayer.create(this, R.raw.deny);
        success = MediaPlayer.create(this, R.raw.success);
        str.start();



    }

    public boolean checkWin(int[] game) {
        boolean g = false;

        for (int[] winningPosition : board.winningPositions) {

            if (game[winningPosition[0]] == game[winningPosition[1]] && game[winningPosition[1]] == game[winningPosition[2]] && game[winningPosition[0]] != 2) {
                // Someone has won!
                g = true;
            }
        }
        return g;
    }

    public void dropIn(View view) {
        ImageView counter = (ImageView) view;
        int tappedCounter = Integer.parseInt(counter.getTag().toString());
        SharedPreferences liveShPref = getApplicationContext().getSharedPreferences("LIVE", Context.MODE_PRIVATE);

        //Toast.makeText(this ,counter.getTag().toString()+" has been touched",Toast.LENGTH_LONG).show();
        TextView turn = (TextView) findViewById(R.id.turnTextView);
        if (board.gameActive && board.numberplayed < 6) {
            drop.start();
            if (board.gameState[tappedCounter] == 2) {

                //Toast.makeText(getBaseContext(),"Minimax: "+ tappedCounter,Toast.LENGTH_LONG).show();
                board.numberplayed++;
                counter.setTranslationY(-1000);
                if (board.activePlayer == 0) {
                    // drop.start();
                    board.drop(tappedCounter, board.activePlayer);
                    p = new Piece(board.activePlayer, tappedCounter);
                    PieceList.add(p);

                    counter.setImageResource(restId);
                    board.activePlayer = 1;

                } else {
                    // drop.start();
                    board.drop(tappedCounter, board.activePlayer);
                    counter.setImageResource(R.drawable.red);
                    p = new Piece(board.activePlayer, tappedCounter);
                    PieceList.add(p);
                    board.activePlayer = 0;
                }
                //counter.animate().translationYBy(1500).rotation(3600).setDuration(300);
                counter.animate().translationYBy(1000).setDuration(200);
            }
        }
        //Yellow player have already played 3 times
        //else if(gameActive && gameState[tappedCounter] == 0 && (activePlayer==0 && numberOfYellowPlayed<3))
        else if ((board.gameActive) && (board.activePlayer == 0)) {
            if (!board.movePiece) {
                if (this.board.gameState[tappedCounter] == player1.playerid) {
                    for (Piece Element : PieceList) {
                        if (Element.position == tappedCounter) {
                            Element.selected = true;
                        }
                    }
                    pastCounter = counter;
                    ptCounter = tappedCounter;
                    board.selectpiece(tappedCounter);
                    if (restId != R.drawable.blue) {
                    counter.setImageDrawable(null); }
                    else
                    { counter.setImageResource(R.drawable.blues); }
                    select.start();
                    moveFrom = tappedCounter;
                    board.movePiece = true;
                }
            } else {
                //see if this move is possible
                if (board.gameState[tappedCounter] == 2 && ((moveFrom == 4 && tappedCounter != 4) || (tappedCounter == board.possiblePositions[moveFrom][0] || tappedCounter == board.possiblePositions[moveFrom][1] || tappedCounter == board.possiblePositions[moveFrom][2]))) {

                    counter.setImageResource(restId);
                    pastCounter.setImageDrawable(null);
                    move.start();
                    board.movepiece(tappedCounter, player1.playerid, player2.playerid);
                    for (Piece Element : PieceList) {
                        if (Element.position == moveFrom) {
                            Element.position = tappedCounter;
                        }
                    }
                } else {
                    //move failed return to previous state
                    board.gameState[moveFrom] = board.activePlayer;
                    pastCounter.setImageResource(restId);
                    deny.start();
                }
                this.moveFrom = -1;
                this.board.movePiece = false;
                for (Piece Element : PieceList) {
                    if (Element.position == tappedCounter) {
                        Element.selected = false;
                    }
                }

            }
        }
        //Red player have already played 3 times
        //else if(gameActive && gameState[tappedCounter] == 1 && (activePlayer==1 && numberOfRedPlayed<3))
        else if (board.gameActive && board.activePlayer == player2.playerid) {
            if (!board.movePiece) {
                if (board.gameState[tappedCounter] == player2.playerid) {
                    board.movePiece = true;
                    pastCounter = counter;
                    board.gameState[tappedCounter] = 2;//this place become free
                    counter.setImageResource(R.drawable.reds);
                    select.start();
                    moveFrom = tappedCounter;
                }

            } else {
                //see if this move is possible
                if (board.gameState[tappedCounter] == 2 && ((moveFrom == 4 && tappedCounter != 4) || (tappedCounter == board.possiblePositions[moveFrom][0] || tappedCounter == board.possiblePositions[moveFrom][1] || tappedCounter == board.possiblePositions[moveFrom][2]))) {
                    counter.setImageResource(R.drawable.red);
                    pastCounter.setImageDrawable(null);
                    move.start();
                    board.movepiece(tappedCounter, player2.playerid, player1.playerid);
                    for (Piece Element : PieceList) {
                        if (Element.position == moveFrom) {
                            Element.position = tappedCounter;
                        }
                    }

                } else {
                    //move failed return to previous state
                    board.gameState[moveFrom] = player2.playerid;
                    pastCounter.setImageResource(R.drawable.red);
                    deny.start();
                }
                moveFrom = -1;
                board.movePiece = false;
            }
        }
        if (board.activePlayer == 0) {
            //turn.setText("     Yellow's turn");
            turn.setText("     blue's turn");
        } else {
            //turn.setText("Red's turn");
            turn.setText("     red's turn");
        }
        //if(!gameActive)turn.setVisibility(view.INVISIBLE);


        if (checkWin(board.gameState)) {
            board.gameActive = false;
            // Button playAgainButton = (Button) findViewById(R.id.playAgainButton);

            TextView winnerTextView = findViewById(R.id.winnerTextView);

            String winner;

            if (board.activePlayer == 1) {

                //winner = "Yellow";
                winner = "blue";
                final SharedPreferences settings = getSharedPreferences("PREFS", Context.MODE_PRIVATE);
                int coin = settings.getInt("COINS", 100);

                SharedPreferences.Editor sett = settings.edit();
                sett.putInt("COINS", coin+20);
                sett.commit();

            } else {

                // winner = "Red";
                winner = "red";
                SharedPreferences.Editor editorimg = liveShPref.edit();
               // int v=liveShPref.getInt("NLIVE", 1);
                editorimg.putInt("NLIVE", MainActivity.nlive -1);
                editorimg.commit();
               // Log.e("jfkf", String.valueOf(v));
                if (liveShPref.getInt("NLIVE", 1)<=0){
                    Intent intent = new Intent(getApplicationContext(), LivesActivity.class);
                    finish();
                    startActivity(intent);
                }

            }

            winnerTextView.setText(winner + " has won!");
            success.start();

            //playAgainButton.setVisibility(View.VISIBLE);

            winnerTextView.setVisibility(View.VISIBLE);
            turn.setVisibility(View.INVISIBLE);
        }


    }

    public void playAgain(View view) {

        str.start();

        //Button playAgainButton = (Button) findViewById(R.id.playAgainButton);

        TextView winnerTextView = (TextView) findViewById(R.id.winnerTextView);

        //playAgainButton.setVisibility(View.INVISIBLE);

        winnerTextView.setVisibility(View.INVISIBLE);
        TextView turn = (TextView) findViewById(R.id.turnTextView);

        turn.setVisibility(view.VISIBLE);
        turn.setText("     blue's turn");

        GridLayout gridLayout = (GridLayout) findViewById(R.id.gridLayout);

        for (int i = 0; i < gridLayout.getChildCount(); i++) {

            ImageView counter = (ImageView) gridLayout.getChildAt(i);

            counter.setImageDrawable(null);

        }

        for (int i = 0; i < board.gameState.length; i++) {

            board.gameState[i] = 2;

        }

        board.activePlayer = 0;

        board.gameActive = true;

        board.numberplayed = 0;
        // roundCounter=0;

    }


}
