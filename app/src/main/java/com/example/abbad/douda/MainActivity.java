package com.example.abbad.douda;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;




public class MainActivity extends AppCompatActivity {

    private NotificationManagerCompat notificationManager;

    public static int coins,nlive;
    TextView tvcoins;
    public TextView tvlive;
    Long timeStamp;
    private boolean Ok=false;

    @SuppressLint("WrongConstant")
    public void clickVsm(View view){

        if (nlive>0) {
            Intent intent = new Intent(getApplicationContext(), vsAiActivity.class);
            startActivity(intent);
        }else {
            Toast.makeText(MainActivity.this, "Not Enough Lives",10).show();
        }
    }

    @SuppressLint("WrongConstant")
    public void clickVsPlayer(View view)
    {
        if (nlive>0) {
            Intent intent = new Intent(getApplicationContext(), vsPlayerActivity.class);
            startActivity(intent);
        }else {
            Toast.makeText(MainActivity.this, "Not Enough Lives",10).show();
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        notificationManager = NotificationManagerCompat.from(this);
        createNotificationChannel();
        final SharedPreferences liveShardPref = getSharedPreferences("LIVE", Context.MODE_PRIVATE);
        nlive = liveShardPref.getInt("NLIVE", 1);
        // getSupportActionBar().hide();
        setContentView(R.layout.activity_main);

        tvcoins= (TextView) findViewById(R.id.tv_coins);
        tvlive= (TextView) findViewById(R.id.textView2);
        Intent soundIntent = new Intent(this, PlaySoundService.class);
        startService(soundIntent);
        SharedPreferences settings = getApplicationContext().getSharedPreferences("PREFS", Context.MODE_PRIVATE);
        coins = settings.getInt("COINS", 100);
        Log.e("rgtft", String.valueOf(coins));

        tvlive.setText(String.valueOf(nlive));
        tvcoins.setText(String.valueOf(coins));




    }



    String CHANNEL_ID = "Hello Preferences";
    public void createNotification() {

        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this,CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setContentTitle("Extra Lives")
                .setContentText("You have 3 Extra lives, You can play another 3 free game ")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                // Set the intent that will fire when the user taps the notification
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

        notificationManager.notify(0, builder.build());

    }
    public void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.app_name);
            String description = "description of notification channel";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }


    public void Clickshop(View view) {
        Intent intent = new Intent(getApplicationContext(), ShopActivity.class);
        startActivity(intent);
    }

    public void ClickTosell(View view) {
        Intent intent = new Intent(getApplicationContext(), DiamondActivity.class);
        startActivity(intent);
    }

    public void clickToSellLive(View view) {
        Intent intent = new Intent(getApplicationContext(), LivesActivity.class);
        startActivity(intent);
    }



    @Override
    protected void onResume() {

        super.onResume();
        tvlive= (TextView) findViewById(R.id.textView2);
        SharedPreferences settings = getApplicationContext().getSharedPreferences("PREFS", Context.MODE_PRIVATE);
        coins = settings.getInt("COINS", 100);
        tvcoins.setText(String.valueOf(coins));
        final SharedPreferences timeStampSaPref = getSharedPreferences("TIME", Context.MODE_PRIVATE);

        final SharedPreferences liveShardPref = getSharedPreferences("LIVE", Context.MODE_PRIVATE);
        nlive = liveShardPref.getInt("NLIVE", 2);
        tvlive.setText(String.valueOf(nlive));

        if (nlive <=0){
            if (Ok==false) {
                Long tsLong = System.currentTimeMillis() / 1000;
                SharedPreferences.Editor editortime = timeStampSaPref.edit();
                editortime.putLong("TIME", tsLong);

                editortime.commit();
            }
            Ok=true;

        }else{
            Ok=false;
        }

        if (Ok==true){
            Long curentTime = System.currentTimeMillis() / 1000;
            Long prevTime=timeStampSaPref.getLong("TIME",1);
            Log.e("prevTime", String.valueOf(prevTime));
            Log.e("CurrentTime", String.valueOf(curentTime));

            if (curentTime-prevTime>4){
                SharedPreferences liveShPref = getApplicationContext().getSharedPreferences("LIVE", Context.MODE_PRIVATE);

                SharedPreferences.Editor editorimg = liveShPref.edit();
                editorimg.putInt("NLIVE", nlive+3);

                editorimg.commit();
                tvlive.setText(String.valueOf(liveShPref.getInt("NLIVE",3)));
                createNotification();
            }
        }

    }



}









