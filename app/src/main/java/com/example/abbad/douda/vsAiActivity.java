package com.example.abbad.douda;



import android.content.Intent;
import android.os.Handler;



import android.os.Bundle;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;


import androidx.appcompat.app.AppCompatActivity;


public class vsAiActivity extends AppCompatActivity {



    public Boolean internet = true;
    public static String nickname;
    public static String roomId;
    public static int[] roomList = {5,5,5,5};
    String[] list = {"","","",""};
    public Boolean joined = false;
    int counter = 0;
    public void onstart() {

// get the nickame of the user




//connect you socket client to the server

        try

        {

            GameActivity.socket = IO.socket("http://192.168.137.75:8000");

            //create connection

            GameActivity.socket.connect();

// emit the event join along side with the nickname

        } catch (
                URISyntaxException e)

        {
            e.printStackTrace();

        }

    }
    @Override
    public void onBackPressed() {
        //start activity here
        counter++;
        final Handler handler = new Handler();
        if (counter == 1) {
            Toast.makeText(vsAiActivity.this,"press second time to exit",Toast.LENGTH_SHORT).show();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (counter == 2) {  System.exit(0); counter =0; }
                    else {counter = 0 ;}



                }
            }, 1000); }



    }
    public  void join(View view) {
        TextView tex= (EditText)findViewById(R.id.editText);
        nickname = tex.getText().toString();
        TextView ide= (EditText)findViewById(R.id.et2);
        String roomid = ide.getText().toString();
        roomId = roomid;
        roomList[0] = 5;
        GameActivity.socket.emit("getTable");
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (roomList[0] == 5) { Toast.makeText(vsAiActivity.this,"Please verify your internet connection",Toast.LENGTH_SHORT).show(); internet = false; }
                else  {
                    if (roomId.equals("0") || roomId.equals("1") || roomId.equals("2") || roomId.equals("3")) {
                        if (!list[Integer.parseInt(roomId)].contains("\"" + nickname + "\":1")  ) {

                            if (2 < 1) {

                                Toast.makeText(vsAiActivity.this,"Incorrect game ID",Toast.LENGTH_SHORT).show();
                            }
                            else if (roomList[Integer.parseInt(roomId)] > 1) {
                                Toast.makeText(vsAiActivity.this,"FULL game",Toast.LENGTH_SHORT).show();

                            }
                            else {
                                roomList[Integer.parseInt(roomId)]++;
                                GameActivity.socket.emit("addnewplayer" , roomId , nickname) ;
                                Intent i  = new Intent(vsAiActivity.this,GameActivity.class);
                                startActivity(i);
                            } }  else {Toast.makeText(vsAiActivity.this,"Nickname already used",Toast.LENGTH_SHORT).show();} }
                } } }, 1000); }



    public  void create(View view) {
        roomList[0] = 5;
        int t;
        GameActivity.socket.emit("getTable");
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (roomList[0] == 5) { Toast.makeText(vsAiActivity.this,"Please verify your internet connection",Toast.LENGTH_SHORT).show(); internet = false; }
                else  {
                    TextView tex= (EditText)findViewById(R.id.editText);
                    nickname = tex.getText().toString();
                    for (int i=0; i<4 ; i++) {
                        if (roomList[i] == 0) { roomId = Integer.toString(i); break; }
                    }
                    if (roomId.equals("0") || roomId.equals("1") || roomId.equals("2") || roomId.equals("3")) {
                        if (!list[Integer.parseInt(roomId)].contains("\"" + nickname + "\":1")  )
                        {
                            GameActivity.socket.emit("addnewplayer", roomId , nickname);
                            Intent i = new Intent(vsAiActivity.this, GameActivity.class);
                            startActivity(i);
                        } else {Toast.makeText(vsAiActivity.this,"Nickname already used",Toast.LENGTH_SHORT).show();}} }
            }
        }, 1000);

    }
    public void listen() {
        GameActivity.socket.on("table", new Emitter.Listener()

        {
            public void call(final Object... args) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        JSONObject data = (JSONObject) args[0];
                        //extract data from fired event
                        try {

                            String ch = data.getString("0");
                            roomList[0] = Integer.parseInt(ch);
                            String ch1 = data.getString("1");
                            roomList[1] = Integer.parseInt(ch1);
                            String ch2 = data.getString("2");
                            roomList[2] = Integer.parseInt(ch2);
                            String ch3 = data.getString("3");
                            roomList[3] = Integer.parseInt(ch3);
                       //     Toast.makeText(vsAiActivity.this,ch,Toast.LENGTH_SHORT).show();
                            //extract data from fired event
                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                } );

            }
        });

        GameActivity.socket.on("existingPlayers", new Emitter.Listener()

        {
            public void call(final Object... args) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        JSONObject data = (JSONObject) args[0];
                        //extract data from fired event
                        try {
                            for (int u=0;u<4;u++) {
                                String ch = data.getString(Integer.toString(u));
                                list[u] = ch;
                            }

                        }

                        //extract data from fired event

                        catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                } );

            }
        });
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vs_ai);
        onstart();
        listen();
        GameActivity.socket.emit("getTable");
        joined = false;
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (roomList[0] == 5) { Toast.makeText(vsAiActivity.this,"Please verify your internet connection",Toast.LENGTH_SHORT).show(); internet = false; }
            }
        }, 2000);
    }

    @Override
    public void onResume() {
        super.onResume();
        setContentView(R.layout.activity_vs_ai);
        // onstart();
        listen();
        GameActivity.socket.emit("getTable");
        joined = false;
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (roomList[0] == 5) { Toast.makeText(vsAiActivity.this,"Please verify your internet connection",Toast.LENGTH_SHORT).show(); internet = false; }
            }
        }, 2000);
    }


}

