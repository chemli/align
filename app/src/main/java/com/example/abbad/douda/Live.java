package com.example.abbad.douda;


import java.util.ArrayList;
import java.util.List;

interface ConnectionBooleanChangedListener {
    public void OnMyBooleanChanged();
}
public class Live {


    public static int Nlive;
    private static List<ConnectionBooleanChangedListener> listeners = new ArrayList<ConnectionBooleanChangedListener>();

    public static int getMyBoolean() { return Nlive; }

    public static void setMyBoolean(int value) {
        Nlive = value;

        for (ConnectionBooleanChangedListener l : listeners) {
            l.OnMyBooleanChanged();
        }
    }

    public static void addMyBooleanListener(ConnectionBooleanChangedListener l) {
        listeners.add(l);
    }
}
