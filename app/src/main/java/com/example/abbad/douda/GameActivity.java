package com.example.abbad.douda;




import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import org.json.JSONException;
import org.json.JSONObject;




import java.util.ArrayList;
import java.util.List;

import com.github.nkzawa.emitter.Emitter;


import com.github.nkzawa.socketio.client.Socket;

import androidx.annotation.DrawableRes;
import androidx.appcompat.app.AppCompatActivity;


import static com.example.abbad.douda.vsAiActivity.nickname;
import static com.example.abbad.douda.vsAiActivity.roomId;
import static com.example.abbad.douda.vsAiActivity.roomList;


public class GameActivity extends AppCompatActivity {
    int[] Find={R.id.imageView1,R.id.imageView2,R.id.imageView3,R.id.imageView4,R.id.imageView5,R.id.imageView6,R.id.imageView7,R.id.imageView8,R.id.imageView9};
    @DrawableRes int restId;
    MediaPlayer drop= null;
    MediaPlayer select= null;
    MediaPlayer move= null;
    MediaPlayer str= null;
    MediaPlayer success= null;
    MediaPlayer deny= null;
    int i=0;
    long time;
    long jstime;
    String roomid = roomId;
    Board board = new Board();

    public static Socket socket;
    Piece p ;
    List<Piece> PieceList = new ArrayList<Piece>();
    Player player1 = new Player(0);
    Player player2 = new Player(1);
    String ch,ch1,room;
    int moveFrom=-1;
    ImageView pastCounter;
    int ptCounter;
    int myid = 1 ;
    View v;

    public void onstart() {

// get the nickame of the user




//connect you socket client to the server



//if you are using a phone device you should connect to same local network as your laptop and disable your pubic firewall as well



        //create connection


        socket.emit("join" , roomId , nickname);



// emit the event join along side with the nickname






    }
    public void send(View view) {
        TextView tex= (EditText)findViewById(R.id.et);
        String message = tex.getText().toString();
        socket.emit("send" , message , nickname , roomId);
        tex.setText("");
    }
    public boolean checkWin(int[] game) {
        boolean g = false;

        for (int[] winningPosition : board.winningPositions) {

            if (game[winningPosition[0]] == game[winningPosition[1]] && game[winningPosition[1]] == game[winningPosition[2]] && game[winningPosition[0]] != 2) {
                // Someone has won!
                g = true;
            }
        }
        return g;
    }
    Handler h = new Handler();
    int delay = 1*1000; //1 second=1000 milisecond, 1*1000=1seconds
    Runnable runnable;

    @Override
    protected void onResume() {
        //start handler as activity become visible
        super.onResume();
        h.postDelayed( runnable = new Runnable() {
            public void run() {
                //do something
                time = System.currentTimeMillis() ;
                i++;
                //  Toast.makeText(GameActivity.this,Long.toString(time/1000),Toast.LENGTH_SHORT).show();
                socket.emit("Imhere" , nickname, roomId , time/1000);
                if (jstime !=0 && (jstime + 6 < time/1000) ) {
                    Toast.makeText(GameActivity.this,"you are disconnected",Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(GameActivity.this, MainActivity.class);
                    startActivity(i);
                }
                h.postDelayed(runnable, delay);
            }
        }, delay);


    }

    @Override
    protected void onPause() {
        h.removeCallbacks(runnable); //stop handler when activity not visible
        super.onPause();
        socket.emit("disconnected", vsAiActivity.nickname , vsAiActivity.roomId);
    }




    public void dropIn(View view) {

        ImageView counter = (ImageView) view;
        int tappedCounter = Integer.parseInt(counter.getTag().toString());
        //Toast.makeText(this ,counter.getTag().toString()+" has been touched",Toast.LENGTH_LONG).show();
        TextView turn = (TextView) findViewById(R.id.turnTextView);
        if (board.gameActive && board.activePlayer==0 && board.numberplayed < 6) {
            drop.start();
            if (board.gameState[tappedCounter] == 2) {
                //Toast.makeText(getBaseContext(),"Minimax: "+ tappedCounter,Toast.LENGTH_LONG).show();
                board.numberplayed++;
                counter.setTranslationY(-1000);
                if (board.activePlayer == 0) {
                    //   ImageView ctr = (ImageView) findViewById(R.id.imageView1 + tappedCounter);
                    board.drop(tappedCounter,0);
                    p = new Piece(board.activePlayer, tappedCounter);
                    PieceList.add(p);
                    counter.setImageResource(restId);

                    board.activePlayer = 1;
                    socket.emit("messagedetection" , Integer.toString(tappedCounter) ,Integer.toString(tappedCounter) , roomid);
                }
                //counter.animate().translationYBy(1500).rotation(3600).setDuration(300);
                counter.animate().translationYBy(1000).setDuration(200);
            }
        }
        //Yellow player have already played 3 times
        //else if(gameActive && gameState[tappedCounter] == 0 && (activePlayer==0 && numberOfYellowPlayed<3))
        else if ((board.gameActive) && (board.activePlayer == 0)) {
            if (!board.movePiece) {
                if (this.board.gameState[tappedCounter] == 0) {
                    for (Piece Element : PieceList) {
                        if (Element.position == tappedCounter) { Element.selected = true; } }
                    pastCounter = counter;
                    ptCounter = tappedCounter;
                    board.selectpiece(tappedCounter);
                  //  counter.setImageResource(R.drawable.blues);
                    if (restId != R.drawable.blue) {
                        counter.setImageDrawable(null); }
                    else
                    { counter.setImageResource(R.drawable.blues); }
                    select.start();
                    board.gameState[tappedCounter] = 2;
                    moveFrom = tappedCounter;
                    board.movePiece = true;
                }
            } else {
                //see if this move is possible
                if (board.gameState[tappedCounter] == 2 && ((moveFrom == 4 && tappedCounter != 4) || (tappedCounter == board.possiblePositions[moveFrom][0] || tappedCounter == board.possiblePositions[moveFrom][1] || tappedCounter == board.possiblePositions[moveFrom][2]))) {
                    move.start();
                    counter.setImageResource(restId);
                    pastCounter.setImageDrawable(null);
                    board.movepiece(tappedCounter,player1.playerid,player2.playerid);
                    socket.emit("messagedetection1" , moveFrom , tappedCounter , roomid) ;
                    for (Piece Element : PieceList) {
                        if (Element.position == moveFrom) { Element.position = tappedCounter; } }
                } else {
                    //move failed return to previous state
                    deny.start();
                    board.gameState[moveFrom] = board.activePlayer;
                    pastCounter.setImageResource(restId);
                }
                this.moveFrom = -1;
                this.board.movePiece = false;
                for (Piece Element : PieceList) {
                    if (Element.position == tappedCounter) { Element.selected = false; } }

            }
        } /*
        //Red player have already played 3 times
        //else if(gameActive && gameState[tappedCounter] == 1 && (activePlayer==1 && numberOfRedPlayed<3))
       else if (board.gameActive && board.activePlayer == player2.playerid) {
            if (!board.movePiece) {
                if (board.gameState[tappedCounter] == player2.playerid) {
                     board.movePiece = true;
                     pastCounter = counter;
                    board.gameState[tappedCounter] = 2;//this place become free
                    counter.setImageDrawable(null);
                    moveFrom = tappedCounter;
                }

            } else {
                //see if this move is possible
                if (board.gameState[tappedCounter] == 2 && ((moveFrom == 4 && tappedCounter != 4) || (tappedCounter == board.possiblePositions[moveFrom][0] || tappedCounter == board.possiblePositions[moveFrom][1] || tappedCounter == board.possiblePositions[moveFrom][2]))) {
                    counter.setImageResource(R.drawable.octupus);
                    board.movepiece(tappedCounter,player2.playerid,player1.playerid);
                    socket.emit("messagedetection1" , Integer.toString(moveFrom) ,Integer.toString(tappedCounter) );
                    for (Piece Element : PieceList) {
                        if (Element.position == moveFrom) { Element.position = tappedCounter; } }

                } else {
                    //move failed return to previous state
                    board.gameState[moveFrom] = player2.playerid;
                    pastCounter.setImageResource(R.drawable.octupus);
                }
                moveFrom = -1;
                board.movePiece = false;
            }
        } */
        if (board.activePlayer == 0) {
            //turn.setText("     Yellow's turn");
            turn.setText("YOUR TURN");
        } else {
            //turn.setText("Red's turn");
            turn.setText("OPPONENT TURN");
        }
        //if(!gameActive)turn.setVisibility(view.INVISIBLE);


        if (checkWin(board.gameState)) {
            board.gameActive = false;
            // Button playAgainButton = (Button) findViewById(R.id.playAgainButton);

            TextView winnerTextView = findViewById(R.id.winnerTextView);



            final SharedPreferences settings = getSharedPreferences("PREFS", Context.MODE_PRIVATE);
            int coin = settings.getInt("COINS", 100);
            SharedPreferences.Editor sett = settings.edit();
            sett.putInt("COINS", coin+20);
            sett.commit();
            winnerTextView.setText("YOU WIN!");
            success.start();

            //playAgainButton.setVisibility(View.VISIBLE);

            winnerTextView.setVisibility(View.VISIBLE);
            turn.setVisibility(View.INVISIBLE);
        }



    }
    public void playAgain(){

        str.start();
        GridLayout gridLayout = (GridLayout) findViewById(R.id.gridLayout);

        for (int i = 0; i < gridLayout.getChildCount(); i++) {

            ImageView counter = (ImageView) gridLayout.getChildAt(i);

            counter.setImageDrawable(null);
            counter = (ImageView) findViewById(Find[i]);
            counter.setImageDrawable(null);

        }

        for (int i = 0; i < board.gameState.length; i++) {

            board.gameState[i] = 2;

        }


        board.activePlayer = 0;

        board.gameActive = true;

        board.numberplayed=0;
      //  socket.emit("playAgain" );
    }
    public void playAgain(View view) {
        str.start();

        //Button playAgainButton = (Button) findViewById(R.id.playAgainButton);

        TextView winnerTextView = (TextView) findViewById(R.id.winnerTextView);

        //playAgainButton.setVisibility(View.INVISIBLE);

        winnerTextView.setVisibility(View.INVISIBLE);
        TextView turn = (TextView) findViewById(R.id.turnTextView);

        turn.setVisibility(view.VISIBLE);
        turn.setText("YOUR TURN");

        GridLayout gridLayout = (GridLayout) findViewById(R.id.gridLayout);

        for (int i = 0; i < gridLayout.getChildCount(); i++) {

            ImageView counter = (ImageView) gridLayout.getChildAt(i);

            counter.setImageDrawable(null);

        }

        for (int i = 0; i < board.gameState.length; i++) {

            board.gameState[i] = 2;

        }


        board.activePlayer = 0;

        board.gameActive = true;

        board.numberplayed=0;
        socket.emit("playAgain" );
        // roundCounter=0;

    }
    public void listen() {
        socket.on("message", new Emitter.Listener()

        {
            public void call(final Object... args) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        JSONObject data = (JSONObject) args[0];
                        //extract data from fired event
                        try {
                            ch = data.getString("message");
                            String ch1 = data.getString("senderNickname");
                            room = data.getString("roomid");
                            if (room.equals(roomid)) {
                                board.gameActive = true;
                                int x = Integer.parseInt(ch);
                                board.drop(x,1 );
                                p = new Piece(board.activePlayer, x);
                                PieceList.add(p);
                                ImageView counter = (ImageView) findViewById(Find[x]);
                                counter.setImageResource(R.drawable.red);
                                board.activePlayer = 0 ;
                                board.numberplayed++;
                                if (checkWin(board.gameState)) {
                                    board.gameActive = false;
                                    // Button playAgainButton = (Button) findViewById(R.id.playAgainButton);

                                    TextView winnerTextView = findViewById(R.id.winnerTextView);

                                    // winner = "Red";
                                    SharedPreferences liveShPref = getApplicationContext().getSharedPreferences("LIVE", Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editorimg = liveShPref.edit();
                                    int v=liveShPref.getInt("NLIVE", 1);
                                    editorimg.putInt("NLIVE", v -1);
                                    editorimg.commit();
                                    // Log.e("jfkf", String.valueOf(v));
                                    if (liveShPref.getInt("NLIVE", 1)<=0){
                                        Intent intent = new Intent(getApplicationContext(), LivesActivity.class);
                                        finish();
                                        startActivity(intent);
                                    }



                                    winnerTextView.setText("GAME OVER!");
                                    deny.start();

                                    //playAgainButton.setVisibility(View.VISIBLE);

                                    winnerTextView.setVisibility(View.VISIBLE);
                                    TextView turn = (TextView) findViewById(R.id.turnTextView);
                                    turn.setVisibility(View.INVISIBLE);
                                }
                            }
                          /*  else {
                                int x = Integer.parseInt(ch1);
                                int y = Integer.parseInt(ch);
                                board.gameState[x] = 2;
                                board.gameState[y] = 0;
                            } */

                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                } );

            }
        });
        socket.on("message1", new Emitter.Listener()

        {
            public void call(final Object... args) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        JSONObject data = (JSONObject) args[0];
                        //extract data from fired event
                        try {
                            String ch = data.getString("message");
                            String ch1 = data.getString("senderNickname");
                            String room = data.getString("roomid");
                            if (room.equals(roomid)) {
                                int x = Integer.parseInt(ch);
                                int y = Integer.parseInt(ch1);
                                ImageView counter = (ImageView) findViewById(Find[x]);
                                counter.setImageResource(R.drawable.red);
                                board.gameState[y] = 2;
                                board.gameState[x] = 1;
                                ImageView counter1 = (ImageView) findViewById(Find[y]);
                                counter1.setImageDrawable(null);
                                board.activePlayer = 0;
                                if (checkWin(board.gameState)) {
                                    board.gameActive = false;
                                    // Button playAgainButton = (Button) findViewById(R.id.playAgainButton);

                                    TextView winnerTextView = findViewById(R.id.winnerTextView);




                                    winnerTextView.setText("GAME OVER!");

                                    //playAgainButton.setVisibility(View.VISIBLE);

                                    winnerTextView.setVisibility(View.VISIBLE);
                                    TextView turn = (TextView) findViewById(R.id.turnTextView);
                                    turn.setVisibility(View.INVISIBLE);
                                }
                            }

                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                } );

            }
        });

        socket.on("userjoinedthechat", new Emitter.Listener()

        {
            public void call(final Object... args) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        JSONObject data = (JSONObject) args[0];
                        //extract data from fired event
                        try {

                            String ch = data.getString("message");
                            String ch1 = data.getString("roomid");

                            //extract data from fired event
                            if (ch1.equals(roomid)) {
                                //   gameActive[Integer.parseInt(roomid)] = true;
                                board.gameActive =true;
                                roomList[Integer.parseInt(roomId)] = 2;
                                playAgain();
                                Toast.makeText(GameActivity.this,ch,Toast.LENGTH_SHORT).show();
                            } }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                } );

            }
        });

        socket.on("playAgain", new Emitter.Listener()

        {
            public void call(final Object... args) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                    /*    for (int i = 0; i < 9; i++) {

                            ImageView counter1 = (ImageView) findViewById(R.id.imageView1 + i);
                            counter1.setImageDrawable(null);

                        } */
                    playAgain();

                        for (int i = 0; i < board.gameState.length; i++) {

                            board.gameState[i] = 2;

                        }


                        board.activePlayer = 0;

                        board.gameActive = true;

                        board.numberplayed=0;
                    }
                } );

            }
        });
        socket.on("userdisconnect", new Emitter.Listener()

        {
            public void call(final Object... args) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        JSONObject data = (JSONObject) args[0];
                        //extract data from fired event
                        try {

                            String ch = data.getString("message");
                            String ch1 = data.getString("roomid");

                            //extract data from fired event
                            if (ch1.equals(roomid)) {
                                Toast.makeText(GameActivity.this,ch,Toast.LENGTH_SHORT).show();
                                Intent i = new Intent(GameActivity.this, MainActivity.class);
                                startActivity(i);

                            } }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                } );

            }
        });
        socket.on("disc", new Emitter.Listener()

        {
            public void call(final Object... args) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        int data = (Integer) args[0];
                        //extract data from fired event



                        jstime = data;




                        //extract data from fired event


                    }
                } );

            }
        });
        socket.on("send", new Emitter.Listener()

        {
            public void call(final Object... args) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        JSONObject data = (JSONObject) args[0];
                        //extract data from fired event
                        try {

                            String ch = data.getString("message");
                            String ch1 = data.getString("Nickname");
                            String ch2 = data.getString("roomid");
                            if (ch2.equals(roomId)) {
                                Toast.makeText(GameActivity.this,ch1 + " : " + ch,Toast.LENGTH_SHORT).show();
                            }
                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }



                        //extract data from fired event


                    }
                } );

            }
        });

    }
    public  void block() {
        for (int i = 0; i < 9; i++) {

            ImageView counter1 = (ImageView) findViewById(R.id.imageView1 + i);
            counter1.setImageDrawable(null);

        }

        for (int i = 0; i < board.gameState.length; i++) {

            board.gameState[i] = 2;

        }


        board.activePlayer = 0;

        board.gameActive = false;

        board.numberplayed=0;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        // getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        board.gameActive = false;
        onstart();
        listen();
        TextView tur = (TextView) findViewById(R.id.tv);
        tur.setText(roomId);
        TextView scor = (TextView) findViewById(R.id.score);
        final SharedPreferences settings = getSharedPreferences("PREFS", Context.MODE_PRIVATE);
        int coin = settings.getInt("COINS", 100);
        scor.setText(String.valueOf(coin));
        drop = MediaPlayer.create(this, R.raw.drop);
        select = MediaPlayer.create(this, R.raw.select);
        move = MediaPlayer.create(this, R.raw.mov);
        str = MediaPlayer.create(this, R.raw.str);
        deny = MediaPlayer.create(this, R.raw.deny);
        success = MediaPlayer.create(this, R.raw.success);
        final SharedPreferences prefImg = getSharedPreferences("IMG", Context.MODE_PRIVATE);
        restId=prefImg.getInt("img",R.drawable.blue );


    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
         // socket.emit("disconnected", vsAiActivity.nickname , vsAiActivity.roomId);
    }


}

